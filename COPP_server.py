# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

from .osc4py3_buildparse import oscbuildparse as oscparse
from collections import namedtuple
import socket
import queue
from collections import deque
import threading
import select
import logging
from re import compile as compile_regex

from . import defaults

# ===========================================================
# =		   SPECIALIZATION OF OSC4PY3 decoders			=
# ===========================================================

def _decode_substr_raw(rawoscdata, index):
	"""This function performs the exact same task as 
	osc4py3.oscbuildparse._decode_str, but takes substring starting index
	as an argument and returns the raw memorybuffer slice.
	This way it is sligtly more performant for us.
	"""
	# Search first zero byte.
	rawoscdata = rawoscdata[ index: ]
	for zeroindex, char in enumerate(rawoscdata):
		if char in (b'\000', 0):
			break
	else:
		raise oscparse.OSCInvalidRawError("OSC non terminated string in raw data for " \
						 "{}".format(oscparse._dumpmv(rawoscdata)))

	# Align for 0 to 3 padding bytes after the zero end-of-string.
	byteslength = (zeroindex // 4) * 4 + 4
	if byteslength > len(rawoscdata):
		raise oscparse.OSCInvalidRawError("OSC invalid align/length for string in "\
							"raw data for {}".format(oscparse._dumpmv(rawoscdata)))
	
	return byteslength, rawoscdata[:zeroindex]


class _OSCMessage(namedtuple('OSCMessage', 'addrpattern typetags payload')):
	pass


class COPP_Message(_OSCMessage):
	__slots__ = ()
	_in_bundle = None
	address_check = compile_regex(r'/\w+$') 
	regex_pre_pattern = r'{}/(\w+)/?([\w_-]+)?/?(.*)?'
	matcher = compile_regex(regex_pre_pattern.format(defaults.BASE_OSC_ADDR))

	@classmethod
	def validate_address(cls, addr):
		if not cls.address_check.match(addr):
			raise ValueError("The new OSC addrpattern: '{}' has a wrong format".format(addr))

	@classmethod
	def update_matcher(cls, val = defaults.BASE_OSC_ADDR ):
		cls.validate_address(val)
		cls.matcher = compile_regex(cls.regex_pre_pattern.format(val))

	@property
	def target(self):
		if not self._in_bundle:
			self._parse_target()
			return self._target

		return self._in_bundle.target

	@property
	def subtarget(self):
		if not self._in_bundle:
			self._parse_target()
		elif not hasattr(self, "_subtarget"):
			self._subtarget = self.addrpattern[ self._in_bundle.subtarget_offset : ]
		
		return self._subtarget

	@property
	def addrtail(self):
		if self.target != defaults.DataTarget.EXTRA:
			return None

		if not hasattr(self, "_addrtail"):
			self._parse_target()
		
		return self._addrtail

	@property
	def arguments(self):
		return self.payload

	def _parse_target(self):
		if not hasattr(self, "_target"):
			res = self.matcher.match(self.addrpattern)
			if not res:
				if self.addrpattern.find(defaults.BASE_OSC_ADDR) == 0:
					raise COPPInvalidRawError("The received COPP message contains an invalid addrpattern = {}".format(self.addrpattern))
				else:
					raise COPPWrongBaseAddress("The received COPP message contains an invalid base address = {}. Expecting: {}".format(self.addrpattern, defaults.BASE_OSC_ADDR ))
				
			try:
				self._target = defaults.DataTarget(res.group(1))
				self._subtarget = res.group(2)
				self._addrtail = res.group(3)
			except ValueError:
				raise COPPInvalidRawError("The received COPP message contains an unknown DataTarget = {}".format(self.addrpattern))

		if defaults.COPP_CHECK_MESSAGES:
			self.check_message()

	def check_message(self):
		if self._target is defaults.DataTarget.EXTRA:
			return

		if self.typetags != defaults.target_typetag_pairs[self._target]:
			raise COPPInvalidRawError("Message doesn't match target's typetag <{}> != <{}> ('{}' target)"
										.format(self.typetags, defaults.target_typetag_pairs[self._target], self._target))

	def __str__(self):
		return "{:>10}-> {:<6} <{}> {}".format(
			self.target, 
			str(self.subtarget), 
			self.typetags[1:], 
			self.payload)

	def get(self):
		yield self
		return

	def restore(self):
		return self


#monkey patch osc4py3's OSCMessage
oscparse.OSCMessage = COPP_Message

# update matcher when global osc address changes
defaults.Global_Parameters_handler.on_osc_address_update.append(COPP_Message.update_matcher)



def _decode_bundle_elements(copp_bundle, allow_nested = defaults.COPP_ALLOW_NESTED_BUNDLES):
	"""This functions performs a similar function than osc4py3.oscbuildparse._decode_bundle
	without the bundlehead cheking and timetag extraction
	"""
	rawoscdata = copp_bundle.raw_payload
	timetag = copp_bundle.time

	oob = {}
	# Will process the whole bundle, element by element.
	totalcount = 0
	elements = []
	elemcount = 0
	while len(rawoscdata):
		elemcount += 1
		# Get size of next element.
		count, size = oscparse._decode_osc_type(rawoscdata, oscparse.OSCTYPE_INT32, oob)
		if size + count > len(rawoscdata):
			raise oscparse.OSCInvalidRawError("OSC invalid bundle element {} size "\
					"in message: size {} for remaining {} "\
					"bytes, size&data: {}".format(elemcount, size,
						len(rawoscdata) - count, oscparse._dumpmv(rawoscdata)))
		totalcount += count
		rawoscdata = rawoscdata[count:]

		subpart = rawoscdata[:size]
		if allow_nested:
			count, elem = oscparse._decode_element(subpart, oob)
		else:
			count, elem = oscparse._decode_message(subpart, oob)

		elem._in_bundle = copp_bundle
		elements.append(elem)
		totalcount += size
		rawoscdata = rawoscdata[size:]

	return oscparse.OSCBundle(timetag, tuple(elements))

# ======  End of SPECIALIZATION OF OSC4PY3 decoders   =======

class Log_Levels:
	from logging import (
        NOTSET, DEBUG, INFO, WARN,
        WARNING, ERROR, CRITICAL )


class UDP_Server(defaults.Global_Parameters_handler):
	receive_timeout = defaults.UDP_RECV_TIMEOUT
	name = 'UDP_Server'
	log_levels = Log_Levels

	def __init__(self, port = defaults.ports["headless_blender"], logger = None ):
		self.port = port
		self.common_packets  = queue.Queue()
		self.last_error = ""
		self.receiving_thread = threading.Thread( target=self.receive_forever )
		self.keep_receiving = True
		self.errors = 0
		if logger:
			if not isinstance(logger, logging.getLoggerClass()):
				raise TypeError("The logger argument should be of type logging.Logger")
				 
			self.logger = logger
			self.external_logging = True
		else:
			self.logger = logging.getLogger( self.name )
			self.logger.setLevel(self.log_levels.INFO)
			ch = logging.StreamHandler()
			ch.setLevel(self.log_levels.DEBUG)
			formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
			ch.setFormatter(formatter)
			self.logger.addHandler(ch)
			self.external_logging = False

	def set_log_level(self, level):
		self.logger.setLevel(level)

	def start(self):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.bind(("0.0.0.0", self.port))
		self.logger.info("{} listening at port {}".format(self.name, self.port))
		self.sock.setblocking(False)
		self.receiving_thread.start()


	def close_and_wait(self):
		self.keep_receiving = False

		if self.receiving_thread.is_alive():
			self.receiving_thread.join()
		self.logger.info("Receiving thread ended")

		if hasattr(self, "sock") and self.sock.fileno() != -1:
			self.sock.close()
			self.logger.info("Socket closed")

		if not self.external_logging:
			for han in self.logger.handlers:
				self.logger.removeHandler(han)

	def _log_error(self, e):
		self.errors += 1
		self.logger.error(e)


	def receive_forever(self):
		self.logger.info("Receiving thread started")
		
		try:
			#If no errors are received we avoid creating a try frame for
			#every round of the loop.
			while self.keep_receiving:
				self.receive_once()
		except Exception as e:
			#We enter here only if a first error is encountered
			self.logger.warning("First error encountered, switch to error-controled reception")
			self._log_error(e)
			while self.keep_receiving:
				try:
					self.receive_once()
				except Exception as e:
					self._log_error(e)

		self.sock.close()
		self.logger.info("Socket closed")

	def _put(self, data):
		self.common_packets.put(data)

	def receive_once(self):
		ready = select.select([self.sock], [], [], self.receive_timeout)
		if ready[0]:
			data = self.sock.recv( defaults.UDP_BUF_SIZE )
			self._put(data)

	def pending(self):
		return self.common_packets.qsize()

	def get(self):
		if not self.pending():
			self.last_error = "empty queue"
			return False 
		return self.common_packets.get()


# ===========================================
# =		   COPP Implementation		   =
# ===========================================


# -----------  Exceptions  -----------

class COPPInvalidRawError(oscparse.OSCError):
	"""COPP specific data corruption detected on raw data.
	for OSC corruption an OSCInvalidRawError should be used.
	"""
	pass

class COPPWrongBaseAddress(oscparse.OSCError):
	"""COPP wrong base address detected."""
	pass


class COPP_Base_packet:
	class_name_format = "\t< {} >\n"
	output_format = "{:>20} = {}\n"

	def __str__(self):
		if not defaults.EXTENDED_STR_REPR:
			return str(self.__class__)

		out = self.class_name_format.format(self.__class__.__name__ )
		out += self.output_format.format("[time]", self.time)
		out += self.output_format.format("[target]", self.target)
		if hasattr(self, "_elements"):
			out += self.output_format.format("[messages]", len(self._elements))
		else:
			out += self.output_format.format("[raw_payload length]", len(self.raw_payload))

		return out

	def get(self):
		while 1:
			el = self.pop()
			if not el:
				return
			yield el


# -----------  Common packet  -----------

class COPP_Common_packet(COPP_Base_packet):
	def __init__(self):
		self.target = None
		self.time = None
		self.raw_payload = None
		self.payload_start = None
		
		self._elements = deque()
		self._consumed = deque()

		self.session_id = -1

	def _get_elements(self):
		return tuple(self._elements)

	def set_elements(self, q):
		self._elements = q

	def pop(self):
		if len(self._elements):
			first = self._elements.popleft()
			self._consumed.append(first)
			return first

		return False

	def restore(self):
		self._elements = self._consumed + self._elements
		self._consumed.clear()
		return self

# -----------  Lazy packet  -----------

class COPP_Lazy_Packet(COPP_Base_packet):
	begin_bundle = oscparse.BEGIN_BUNDLE[:4]
	OSC_chunk_size = 4
	timetag_start = 2 * OSC_chunk_size
	timetag_end = 4 * OSC_chunk_size
	addr_replacement_mark = b"/%%"
	base_osc_addr = bytes(defaults.BASE_OSC_ADDR + '/', "ascii")
	replacement_target = addr_replacement_mark + base_osc_addr
	replacement_target_start = len(replacement_target )
	payload_start_mark = b"/%"
	subtarget_offset = len(payload_start_mark) + 1 

	def __init__(self, rawoscdata=None):
		self._consumed_elems = 0

		if not rawoscdata:
			return #just as flow control, create empty COPP_Lazy_Packet

		#Check if it's an OSC bundle
		if not rawoscdata.startswith( self.begin_bundle ):
			raise oscparse.OSCInvalidRawError("The received packet is not an OSC bundle")
		
		self._buffer = memoryview(rawoscdata)
		self._parse_timetag()
		payload_start = self._parse_datatarget(rawoscdata)

		payload_start = rawoscdata.find(self.payload_start_mark, payload_start) - 4
		self.raw_payload = self._buffer[ payload_start : ]

		self.session_id = -1



	@classmethod
	def update_base_addr(cls, val = defaults.BASE_OSC_ADDR ):
		COPP_Message.validate_address(val)
		cls.base_osc_addr = bytes(defaults.BASE_OSC_ADDR + '/', "ascii")
		cls.replacement_target = cls.addr_replacement_mark + cls.base_osc_addr
		cls.replacement_target_start = len(cls.replacement_target)		

	# --- PRIVATE METHODS 
	
	def _parse_timetag(self):
		"""Get the timetag of the bundle"""
		try:
			timetag_chunk = self._buffer[self.timetag_start:self.timetag_end]
			count, self.time = oscparse._decode_osc_type(timetag_chunk, oscparse.OSCTYPE_TIMETAG, {})
		except:
			raise oscparse.OSCInvalidRawError("The received packet is not an OSC bundle: the timetag is corrupted")

	def _parse_datatarget(self, rawoscdata):
		"""Look for the COPP replacement mark as first message of the bundle.
		Get the datatarget of the bundle by looking at the address replacement mark.
		This function throws a COPPInvalidRawError if the Datatarget is unknown"""

		addr_replacement_start = rawoscdata.find(self.addr_replacement_mark)
		if addr_replacement_start < 0:
			raise COPPInvalidRawError("The received packet is not an COPP bundle: the replacement address message can't be found")

		count, self.target = _decode_substr_raw(self._buffer, addr_replacement_start )

		if rawoscdata.find(self.replacement_target, addr_replacement_start) != addr_replacement_start:
			raise COPPWrongBaseAddress("The received COPP bundle contains an unknown replacement Base address = {}. Expecting = {}".format(rawoscdata[addr_replacement_start:addr_replacement_start + count], 
				self.replacement_target))

		self.target = self.target[ self.replacement_target_start: ]

		try:
			self.target = defaults.DataTarget(bytes(self.target).decode('ascii', 'strict'))
		except ValueError:
			raise COPPInvalidRawError("The received COPP bundle contains an unknown DataTarget = {}".format(self.target))

		return addr_replacement_start + count

	# --- PUBLIC METHODS

	def _get_elements(self):
		if not hasattr(self, "_bundle"):
			self._bundle = _decode_bundle_elements(self)
			self._n_msgs = len(self._bundle.elements)

		return self._bundle.elements

	def check_message(self, msg):
		if self.target is defaults.DataTarget.EXTRA:
			return

		if msg.typetags != defaults.target_typetag_pairs[self.target]:
			n = self._consumed_elems
			raise COPPInvalidRawError("Message #{} in COPP bundle doesn't match bundle target typetag <{}> != <{}> ('{}' target)"
										.format(n, msg.typetags, defaults.target_typetag_pairs[self.target], self.target))


	def pop(self):
		msg = self._get_elements()
		if self._consumed_elems >= self._n_msgs:
			return False

		msg = msg[self._consumed_elems]

		if defaults.COPP_CHECK_MESSAGES:
			self.check_message(msg)

		self._consumed_elems += 1
		
		return msg

	def restore(self):
		self._consumed_elems = 0
		return self

# update base_addr when global osc address changes
defaults.Global_Parameters_handler.on_osc_address_update.append(COPP_Lazy_Packet.update_base_addr)


# -----------  Common queue  -----------

class COPP_Common_queue():
	def __init__(self):
		self.lock = threading.Lock()
		self.q = deque()

	def put(self, el):
		with self.lock:
			self.q.append(el)

	def get(self):
		with self.lock:
			if not len(self.q):
				return False
			return self.q.popleft()

	def extract(self):
		with self.lock:
			tmp = self.q
			self.q = deque()
			return tmp

	def empty(self):
		return len(self.q) == 0 

	def qsize(self):
		return len(self.q)

# -----------  Server  -----------

class COPP_Server(UDP_Server):
	def __init__(self, port = defaults.ports["headless_blender"], logger = None ):
		self.bundle_packets = queue.Queue()
		self.name = 'COPP_Server'
		super().__init__(port, logger)
		self.common_packets = COPP_Common_queue()
 	
	def _put(self, data):
		try:
			lazy_p = COPP_Lazy_Packet(data)
			self.bundle_packets.put(lazy_p)
			if self.logger.level == self.log_levels.DEBUG:
				self.logger.debug(lazy_p)

		except oscparse.OSCInvalidRawError as e:
			# the packet is not a bundle, parse it as a message
			msg = oscparse._decode_message(data, {})[1]

			self.common_packets.put(msg)
			if self.logger.level == self.log_levels.DEBUG:
				self.logger.debug(msg)

		except COPPInvalidRawError as e:
			if defaults.PARSE_NON_COPP_BUNDLES:
				self._parse_non_copp_bundles(data)				
			else:
				self.logger.error("NON COPP BUNDLE RECEIVED: " + oscparse._dumpmv(data))

		except COPPWrongBaseAddress as e:
			if defaults.PARSE_NON_COPP_BUNDLES:
				self._parse_non_copp_bundles(data)				
			else:
				self.logger.error(e)

		except Exception as e:
			self.logger.critical("Internal server error:\n%s" % e )			

	def _parse_non_copp_bundles(self, data):
		try:
			count, res = oscparse._decode_element(data, {})
			if type(res) == oscparse.OSCBundle:
				for el in res.elements:
					self.common_packets.put(el)

		except Exception as e:
			self.logger.critical("Error while parsing non-copp bundle:\n%s" % e )

	def get(self):
		if not self.common_packets.empty():
			comm = COPP_Common_packet()
			comm.set_elements(self.common_packets.extract())
			yield comm

		while self.bundle_packets.qsize():
			yield self.bundle_packets.get()

		return

	def pending(self):
		return self.bundle_packets.qsize() + int(not self.common_packets.empty())

# ======  End of COPP Implementation  =======

