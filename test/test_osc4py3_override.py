# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from ..osc4py3_buildparse import oscbuildparse as oscparse
from .constants import *
from .. import COPP_server, defaults

def test_decode_substr_raw(OSC_raw_bundle, OSC_raw_bundle_indexes):
	assert OSC_raw_bundle.startswith(b"#bundle")
	oscparse.dumphex_buffer(OSC_raw_bundle)
		
	OSC_chunk_size, position, starting_i, end_i = OSC_raw_bundle_indexes
	assert OSC_raw_bundle[ starting_i : end_i ] == bytes(first_msg_addr, "utf-8")

	pos, res = COPP_server._decode_substr_raw(OSC_raw_bundle, starting_i )
	assert bytes(res).decode('ascii', 'strict') == first_msg_addr
	assert pos == ( len(first_msg_addr) // 4 ) * 4 + 4 # <== 4 aligned lenght

def test_decode_substr_raw_wrong(OSC_raw_bundle, OSC_raw_bundle_indexes):
	OSC_chunk_size, position, starting_i, end_i = OSC_raw_bundle_indexes
	#Non zero terminated string
	with pytest.raises(oscparse.OSCInvalidRawError, match=r".*non terminated string in raw data.*"):
		COPP_server._decode_substr_raw(OSC_raw_bundle[:end_i], starting_i )