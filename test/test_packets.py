# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from ..osc4py3_buildparse import oscbuildparse as oscparse
from .constants import *
from .. import COPP_server, defaults

# -----------  COOP_Lazy_packet contruction  -----------

def test_COPP_Lazy_Packet_raw_packet(OSC_raw_bundle, OSC_payload):
	lazy_p = COPP_server.COPP_Lazy_Packet(OSC_raw_bundle)
	assert lazy_p.time == test_timetag
	assert lazy_p.target == defaults.DataTarget(test_target)

	payload_string = lazy_p.raw_payload.tobytes().decode('ascii', 'replace')
	assert payload_string[4:].startswith(OSC_payload[1][0])

def test_COPP_Lazy_Packet_valid_packets(OSC_raw_bundle, OSC_payload):
	#Inmediate bundle
	inmediate_bundle = OSC_raw_bundle[:7] 
	inmediate_bundle += b'\x00\x00\x00\x00\x00\x00\x00\x00\x01'
	inmediate_bundle += OSC_raw_bundle[20:40]
	oscparse.dumphex_buffer(inmediate_bundle)
	inmediate_Lazy = COPP_server.COPP_Lazy_Packet(inmediate_bundle)
	inmediate_Lazy.time == oscparse.OSC_IMMEDIATELY

def test_COPP_Lazy_Packet_raw_packet_wrong(OSC_raw_bundle, OSC_payload):
	#Not an OSC Bundle
	with pytest.raises(oscparse.OSCInvalidRawError, match=r".*not an OSC bundle.*"):
		COPP_server.COPP_Lazy_Packet(OSC_raw_bundle[1:])

	#Not a COPP bundle (missing addr replacement)
	bun = oscparse.OSCBundle(test_timetag, OSC_payload[0])
	not_copp_bundle = oscparse.encode_packet(bun)
	with pytest.raises(COPP_server.COPPInvalidRawError, match=r".*not an COPP bundle*"):
		COPP_server.COPP_Lazy_Packet(not_copp_bundle)

	#Drastically truncated bundle
	minimal_bundle = b"#bun"
	with pytest.raises(oscparse.OSCInvalidRawError):
		COPP_server.COPP_Lazy_Packet(minimal_bundle)

	#Unknown DataTarget
	find_addr = first_msg_addr.encode("utf-8")
	unknown_DT_bundle = OSC_raw_bundle.replace(find_addr, find_addr[ :-len(test_target)] + b"z" )
	with pytest.raises(COPP_server.COPPInvalidRawError, match=r".*unknown DataTarget.*"):
		COPP_server.COPP_Lazy_Packet(unknown_DT_bundle)

# -----------  COOP_Lazy_packet parsing  -----------

def test_COPP_Lazy_Packet_parsing(OSC_raw_bundle, OSC_payload):
	lazy_p = COPP_server.COPP_Lazy_Packet(OSC_raw_bundle)
	assert lazy_p._get_elements()[0].addrpattern is not None
	assert lazy_p._get_elements()[0].typetags is not None
	assert lazy_p._get_elements()[0].arguments is not None
	assert lazy_p._get_elements()[0]._in_bundle is lazy_p
	assert lazy_p._get_elements()[0].target == defaults.DataTarget(test_target)
	assert lazy_p._get_elements()[0].subtarget == test_subtargets[0]
	assert lazy_p._get_elements()[1].subtarget == test_subtargets[1]
	assert lazy_p._get_elements()[0].payload == pytest.approx(OSC_payload[2][1])

def test_COPP_Lazy_Packet_parse_cache(OSC_raw_bundle, OSC_payload, mocker):
	lazy_p = COPP_server.COPP_Lazy_Packet(OSC_raw_bundle)
	stash_decode_bundle_elements = COPP_server._decode_bundle_elements
	COPP_server._decode_bundle_elements = mocker.MagicMock()
	a = lazy_p._get_elements()
	b = lazy_p._get_elements()
	c = lazy_p._get_elements()
	COPP_server._decode_bundle_elements.assert_called_once()
	assert a == b == c
	assert a is b and b is c
	COPP_server._decode_bundle_elements = stash_decode_bundle_elements


def test_COPP_Lazy_Packet_parse_ckeck(OSC_var_bundle, OSC_payload):
	wrong_msg = [oscparse.OSCMessage('/%/arm', ',ifff', [1,2,3,4] )]
	msgs = OSC_payload[0] + wrong_msg 
	all_q_but_one = OSC_var_bundle( msgs )

	lazy_p = COPP_server.COPP_Lazy_Packet(all_q_but_one)
	with pytest.raises(COPP_server.COPPInvalidRawError, match="Message #3"):
		for subt in msgs:
			lazy_p.pop()

def test_COPP_using_default_ENUM_as_keys():
	for k in defaults.target_typetag_pairs.keys():
		assert type(k) == defaults.DataTarget

def test_COPP_Lazy_Packet_get_and_pop(OSC_raw_bundle, OSC_payload):
	lazy_p = COPP_server.COPP_Lazy_Packet(OSC_raw_bundle)
	for subt in test_subtargets:
		assert lazy_p.pop().subtarget == subt 

	#More pop requests should return False
	assert not lazy_p.pop()

def test_COPP_Lazy_Packet_iteration(OSC_raw_bundle):
	lazy_p = COPP_server.COPP_Lazy_Packet(OSC_raw_bundle)
	elems = []
	for el in lazy_p.get():
		elems.append(el)

	assert len(elems) == len(test_subtargets)

	with pytest.raises(StopIteration):
		lazy_p.get().__next__()

	# Packets should be able to return to their original state
	lazy_p.restore()
	
	for el in lazy_p.get():
		elems.append(el)

	assert len(elems) == len(test_subtargets*2)
	restored_subts = [msg.subtarget for msg in elems[3:]]
	assert restored_subts == test_subtargets

def test_Lazy_Packet_base_adress_change(OSC_raw_bundle):
	lazy_p = COPP_server.COPP_Lazy_Packet(OSC_raw_bundle)
	assert defaults.BASE_OSC_ADDR == "/Chordata"
	b_base_addr = bytes(defaults.BASE_OSC_ADDR, 'ascii')
	assert lazy_p.base_osc_addr.find(b_base_addr) != -1
	assert COPP_server.COPP_Lazy_Packet.base_osc_addr.find(b_base_addr) != -1
	original_addr = defaults.BASE_OSC_ADDR

	assert lazy_p.target == defaults.DataTarget.Q

	# Lazy packet's base addr should be updated when global
	# BASE_OSC_ADDRESS gets changed
	params = defaults.Global_Parameters_handler()
	new_addr = "/Pippo"
	b_new_addr = bytes(new_addr, 'ascii')
	params.set_base_osc_addr( new_addr )
	assert defaults.BASE_OSC_ADDR == new_addr
	assert lazy_p.base_osc_addr.find(b_new_addr) != -1
	assert COPP_server.COPP_Lazy_Packet.base_osc_addr.find(b_new_addr) != -1

	# The new address should be well formated
	new_addr = "Pippo"
	with pytest.raises(ValueError):
		COPP_server.COPP_Lazy_Packet.update_base_addr( new_addr )

	new_addr = "/Pippo/"
	with pytest.raises(ValueError):
		COPP_server.COPP_Lazy_Packet.update_base_addr( new_addr )

	new_addr = "//Pippo"
	with pytest.raises(ValueError):
		COPP_server.COPP_Lazy_Packet.update_base_addr( new_addr )

	# parsing a new lazy packet with the old base address should throw
	with pytest.raises(COPP_server.COPPWrongBaseAddress, match=r".*unknown replacement Base*"):
		lazy_p = COPP_server.COPP_Lazy_Packet(OSC_raw_bundle)

	params.set_base_osc_addr( original_addr )
	


def test_COPP_Common_Packet_iteration():
	com_p = COPP_server.COPP_Common_packet()

	items = 500
	for i in range(items):
		com_p._elements.append("something")

	counter = 0
	for item in com_p.get():
		counter +=1

	assert items == counter	

	with pytest.raises(StopIteration):
		com_p.get().__next__()

def test_COPP_Common_Packet_restore():
	com_p = COPP_server.COPP_Common_packet()

	for st in test_subtargets:
		com_p._elements.append(st)

	counter = 0
	for item in com_p.get():
		counter +=1

	assert len(test_subtargets) == counter	

	com_p.restore()
	
	elems = []
	for el in com_p.get():
		elems.append(el)

	assert len(elems) == len(test_subtargets)
	assert elems == test_subtargets


