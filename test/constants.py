# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

import time
from ..osc4py3_buildparse import oscbuildparse as oscparse

test_message = b'*** This is a test message ***'
test_port = 4567
test_server_address = ('localhost', test_port)


test_subtargets = ["base", "dorsal", "head"]

test_timetag = time.time()
test_timetag = oscparse.unixtime2timetag( 0.001 * test_timetag )
test_target = "q"
first_msg_addr = "/%%/Chordata/" + test_target