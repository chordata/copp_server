# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from ..osc4py3_buildparse import oscbuildparse as oscparse
from .constants import *
from .. import COPP_server, defaults

@pytest.fixture(scope="module")
def OSC_payload():
	defaults.COPP_CHECK_MESSAGES = False
	payload = (',ffff', [0.1, 0.2, 0.3, 0.4])
	subtargets = ['{}/comm/{}'.format(defaults.BASE_OSC_ADDR, sub) for sub in test_subtargets]
	subtargets.append("/Chordata/extra/head/temperature") 				#3
	subtargets.append("/Chord/comm/head")								#4
	subtargets.append("{}/fake/head".format(defaults.BASE_OSC_ADDR))	#5
	subtargets.append("{}//q/head".format(defaults.BASE_OSC_ADDR))	#6
	msgs = [oscparse.OSCMessage(sub, *payload) for sub in subtargets]
	yield msgs, subtargets, payload
	defaults.COPP_CHECK_MESSAGES = True


def test_COPP_message(OSC_raw_payload):
	count, elem = oscparse._decode_message(OSC_raw_payload[0], {})
	assert type(elem) == COPP_server.COPP_Message
	assert OSC_raw_payload[1].startswith(bytes("{}/comm".format(defaults.BASE_OSC_ADDR), 'ascii'))


def test_COPP_message_target(OSC_raw_payload):
	count, elem = oscparse._decode_message(OSC_raw_payload[0], {})
	assert elem.target == defaults.DataTarget.COMM
	assert elem.addrtail is None

	count, elem = oscparse._decode_message(OSC_raw_payload[3], {})
	assert elem.target == defaults.DataTarget.EXTRA
	assert elem.subtarget == "head"
	assert elem.addrtail == "temperature"

def test_COPP_message_subtarget(OSC_raw_payload):
	for i,sub in enumerate(test_subtargets):
		count, elem = oscparse._decode_message(OSC_raw_payload[i], {})
		assert elem.subtarget == sub


def test_COPP_message_wrong(OSC_raw_payload):
	#The message doesn't match the form /(base addr)/(target)/(subtarget)
	count, elem = oscparse._decode_message(OSC_raw_payload[4], {})
	with pytest.raises(COPP_server.COPPWrongBaseAddress, match=r".*invalid base address*"):
		elem.target

	#The target is unknown
	count, elem = oscparse._decode_message(OSC_raw_payload[5], {})
	with pytest.raises(COPP_server.COPPInvalidRawError, match=r".* unknown DataTarget*"):
		elem.target

	#The addrpattern is invalid
	count, elem = oscparse._decode_message(OSC_raw_payload[6], {})
	with pytest.raises(COPP_server.COPPInvalidRawError, match=r".* invalid addrpattern*"):
		elem.target

def test_COPP_message_typetag_check(OSC_raw_payload):
	defaults.COPP_CHECK_MESSAGES = True
	count, elem = oscparse._decode_message(OSC_raw_payload[2], {})
	with pytest.raises(COPP_server.COPPInvalidRawError, match=r".*Message doesn't match target's typetag*"):
		elem.target

	count, elem = oscparse._decode_message(OSC_raw_payload[3], {})
	assert elem.target is defaults.DataTarget.EXTRA

def test_COPP_message_get(OSC_raw_payload):
	# The COPP_Message.get() should return a one-go iterator
	# This is useful to unify the interface
	count, elem = oscparse._decode_message(OSC_raw_payload[2], {})
	iterator = elem.get()
	get_elem = iterator.__next__()
	assert type(get_elem) == COPP_server.COPP_Message
	assert get_elem == elem

	with pytest.raises(StopIteration):
		iterator.__next__()

	for i,el in enumerate(elem.get()):
		pass

	assert i == 0

def test_COPP_message_base_adress(OSC_raw_payload):
	count, elem = oscparse._decode_message(OSC_raw_payload[2], {})
	assert defaults.BASE_OSC_ADDR == "/Chordata"
	original_addr = defaults.BASE_OSC_ADDR
	assert elem.matcher.pattern.find(original_addr) != -1
	assert COPP_server.COPP_Message.matcher.pattern.find(original_addr) != -1
	
	# Pre-compiled regex matcher should be updated when global
	# BASE_OSC_ADDRESS gets changed
	params = defaults.Global_Parameters_handler()
	new_addr = "/Pippo"
	params.set_base_osc_addr( new_addr )
	assert defaults.BASE_OSC_ADDR == new_addr
	assert elem.matcher.pattern.find(new_addr) != -1
	assert COPP_server.COPP_Message.matcher.pattern.find(new_addr) != -1

	# The new address should be well formated
	new_addr = "Pippo"
	with pytest.raises(ValueError):
		params.set_base_osc_addr( new_addr )

	new_addr = "/Pippo/"
	with pytest.raises(ValueError):
		params.set_base_osc_addr( new_addr )

	new_addr = "//Pippo"
	with pytest.raises(ValueError):
		params.set_base_osc_addr( new_addr )

	params.set_base_osc_addr( original_addr )

