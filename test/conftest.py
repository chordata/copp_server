# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

import pytest
import socket
import threading
import time
from .. import COPP_server, defaults
from ..osc4py3_buildparse import oscbuildparse as oscparse

from .constants import *



@pytest.fixture
def udp_server():
	#reduce the timeout to make it close faster during tests
	COPP_server.UDP_Server.receive_timeout = 0.1
	udp_server = COPP_server.UDP_Server( port = test_port )
	
	yield udp_server
	udp_server.close_and_wait()
	if hasattr(udp_server, "sock"):
		assert udp_server.sock.fileno() == -1
	assert not udp_server.receiving_thread.is_alive()

@pytest.fixture
def test_udp_client():
	"""This client sends test mesages forever"""
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	def keep_sending():
		while sock.fileno() != -1:
			sock.sendto(test_message, test_server_address)
			time.sleep(0.05)

	t = threading.Thread(target = keep_sending)
	yield t, sock
	if sock.fileno != -1:
		sock.close()
	
	if t.is_alive():
		t.join()

# ==================================
# =           OSC / COPP           =
# ==================================

@pytest.fixture
def OSC_payload():
	payload = (',ffff', [0.1, 0.2, 0.3, 0.4])
	subtargets = ['/%/{}'.format(sub) for sub in test_subtargets]
	msgs = [oscparse.OSCMessage(sub, *payload) for sub in subtargets]
	return msgs, subtargets, payload

@pytest.fixture
def OSC_raw_payload(OSC_payload):
	msgs = [oscparse.encode_packet(m) for m in OSC_payload[0]]
	return msgs

@pytest.fixture
def OSC_raw_bundle(OSC_payload):
	"""This fisture will output a raw byte sequence like this:
	000:2362756e 646c6500 83c288bb 67d59800     #bun dle. .... g...
	016:00000014 2f25252f 43686f72 64617461     .... /%%/ Chor data
	032:2f710000 2c4e0000 00000020 2f252f62     /q.. ,N.. ...  /%/b
	048:61736500 2c666666 66000000 3dcccccd     ase. ,fff f... =...
	064:3e4ccccd 3e99999a 3ecccccd 00000024     >L.. >... >... ...$
	080:2f252f64 6f727361 6c000000 2c666666     /%/d orsa l... ,fff
	096:66000000 3dcccccd 3e4ccccd 3e99999a     f... =... >L.. >...
	112:3ecccccd 00000020 2f252f68 65616400     >... ...  /%/h ead.
	128:2c666666 66000000 3dcccccd 3e4ccccd     ,fff f... =... >L..
	144:3e99999a 3ecccccd                       >... >...
	"""
	first_msg = [oscparse.OSCMessage(first_msg_addr, ",N", [None])]

	bun = oscparse.OSCBundle(test_timetag, first_msg + OSC_payload[0])
	raw = oscparse.encode_packet(bun)
	return raw

@pytest.fixture
def OSC_var_bundle():
	first_msg = [oscparse.OSCMessage(first_msg_addr, ",N", [None])]

	def create_bundle(payload):
		bun = oscparse.OSCBundle(test_timetag, first_msg + payload)
		raw = oscparse.encode_packet(bun)
		return raw

	return create_bundle

def test_variadic_bundle_fixture(OSC_var_bundle, OSC_raw_bundle, OSC_payload):
	var_bun = OSC_var_bundle(OSC_payload[0])
	assert var_bun == OSC_raw_bundle

@pytest.fixture
def OSC_raw_bundle_indexes():
	OSC_chunk_size = 4
	position = 0
	position += 2 # '#bundle\0' string
	position += 2 # timetag
	position += 1 # first message size
	starting_i =  position * OSC_chunk_size
	end_i = starting_i + len(first_msg_addr)

	return OSC_chunk_size, position, starting_i, end_i