# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from .. import COPP_server, defaults
import socket
import threading
import time
from ..osc4py3_buildparse import oscbuildparse as oscparse

from .constants import *

# -----------  COPP server  -----------

@pytest.fixture
def copp_server():
	#reduce the timeout to make it close faster during tests
	COPP_server.COPP_Server.receive_timeout = 0.1
	copp = COPP_server.COPP_Server( port = test_port )
	
	yield copp
	copp.close_and_wait()
	if hasattr(copp, "sock"):
		assert copp.sock.fileno() == -1
	assert not copp.receiving_thread.is_alive()

@pytest.fixture
def lazy_packet(OSC_raw_bundle):
	lazy_p = COPP_server.COPP_Lazy_Packet(OSC_raw_bundle)

@pytest.fixture
def OSC_comm_message():
	payload = (',s', ["this is a test string message"])
	return oscparse.OSCMessage("/Chordata/comm", *payload), payload

@pytest.fixture
def OSC_raw_comm_message(OSC_comm_message):
	return oscparse.encode_packet(OSC_comm_message[0])
	
def test_COPP_Common_Packet_void_arg():
	com_p = COPP_server.COPP_Common_packet()
	assert com_p.target is None
	assert com_p.time is None
	assert com_p.raw_payload is None

def test_COPP_server_not_copp_bundles(copp_server, OSC_comm_message):
	void_bun = oscparse.OSCBundle(test_timetag, [])
	raw = oscparse.encode_packet(void_bun)
	copp_server._put(raw)
	assert copp_server.pending() == 0

	number_of_msgs = 3
	normal_bun = oscparse.OSCBundle(test_timetag, [OSC_comm_message[0]] * number_of_msgs )
	raw = oscparse.encode_packet(normal_bun)
	copp_server._put(raw)
	assert copp_server.pending() == 0
	copp_server.set_parse_non_copp_bundles(True)
	copp_server._put(raw)
	assert copp_server.pending() == 1
	packet = copp_server.get().__next__()
	assert len(packet._get_elements()) == number_of_msgs
	copp_server.set_parse_non_copp_bundles(False)

def test_COPP_Server_dual_queue(copp_server, OSC_raw_bundle, OSC_raw_comm_message, OSC_comm_message):
	#the queue should start empty
	assert type(copp_server.common_packets) is COPP_server.COPP_Common_queue
	assert not copp_server.pending()

	#three common messages are received and 2 bundles
	copp_server._put(OSC_raw_bundle)
	assert copp_server.pending() == 1
	copp_server._put(OSC_raw_comm_message)
	assert copp_server.pending() == 2
	copp_server._put(OSC_raw_bundle)
	copp_server._put(OSC_raw_comm_message)
	#The pending method should consider all common messages as a unique bundle
	assert copp_server.pending() == 3
	copp_server._put(OSC_raw_comm_message)

	#The COPP server should return the common messages first as a single common packet
	common_packet = copp_server.get().__next__()
	assert common_packet.target is None
	assert len(common_packet._elements) == 3
	assert type(common_packet) is COPP_server.COPP_Common_packet
	print(common_packet._elements)
	assert common_packet.pop().typetags == OSC_comm_message[1][0]
	assert common_packet.pop().payload == tuple(OSC_comm_message[1][1])
	common_packet.pop()
	assert not common_packet.pop()

	#after the common packet the server should return the Lazy ones one at the time
	assert copp_server.get().__next__().target == defaults.DataTarget(test_target)
	assert copp_server.get().__next__().target == defaults.DataTarget(test_target)

	with pytest.raises(StopIteration):
		copp_server.get().__next__()


def test_COPP_server_for_iteration(copp_server, OSC_raw_bundle):
	items = 500
	for i in range(items):
		copp_server._put(OSC_raw_bundle)

	counter = 0
	for item in copp_server.get():
		counter +=1

	assert items == counter


def test_COPP_Server_stability(copp_server, OSC_raw_bundle, test_udp_client):
	copp_server.start()
	client, sock = test_udp_client

	sock.sendto(OSC_raw_bundle[1:], test_server_address)
	time.sleep(0.1)
	assert copp_server.receiving_thread.is_alive()
	assert copp_server.errors == 1
	assert copp_server.pending() == 0
	sock.sendto(OSC_raw_bundle[1:], test_server_address)
	time.sleep(0.1)
	assert copp_server.errors == 2
	sock.sendto(OSC_raw_bundle, test_server_address)
	time.sleep(0.1)
	assert copp_server.pending() == 1
	

# -----------  COPP Common queue  -----------

def test_COPP_Common_queue_basics():
	common_q = COPP_server.COPP_Common_queue()
	common_q.put("a")
	common_q.put("b")
	assert common_q.get() == "a"
	assert common_q.get() == "b"

def test_COPP_Common_queue_extract():
	common_q = COPP_server.COPP_Common_queue()
	common_q.put("a")
	common_q.put("b")
	inner_queue = common_q.q
	extracted = common_q.extract()
	assert extracted is inner_queue
	assert extracted is not common_q.q
	assert extracted.popleft() == "a"
	assert extracted.popleft() == "b"
	assert len(common_q.q) == 0
	assert common_q.get() == False

@pytest.mark.slow
def test_COPP_Common_queue_thread_safe():
	"""Testing race conditions con COPP common queue
	This test spawn a number of threads in parallel that write to the queue.
	The main thread reads the queue and asserts the order of the items is correct
	and no items where lost

	ATTENTION! it is a slow test.
	"""
	import threading
	import time
	import random

	common_q = COPP_server.COPP_Common_queue()
	to_put = ("a", "b", "c", "d")
	rounds = 3000

	# -----------  worker thread task  -----------
	def stress_it():
		name = threading.current_thread().name + " %s" 
		for i in range(rounds):
			time.sleep(random.random() * 0.001)
			for ch in to_put:
				common_q.put(name % ch)

	# -----------  Spawn threads  -----------
	thread_N = 10
	threads = [threading.Thread(target=stress_it, name="t_%d"%i) for i in range(thread_N)]
	for t in threads:
		t.start()

	# -----------  Main thread task  -----------
	count = 0
	def extract_and_assert():
		nonlocal count, common_q
		extracted = common_q.extract()
		if extracted:
			count += len(extracted)
			first = extracted.popleft()
			assert first[-1] in to_put
			if extracted:
				# second = extracted.popleft()
				should_be = (to_put.index(first[-1]) + 1) % 4
				for entry in extracted:
					if entry[:-1] == first[:-1]:
						assert entry[-1] == to_put[should_be]
						break

	# -----------  Main thread loop  -----------
	while 1:
		do_close = True
		for t in threads:
			if t.is_alive():
				do_close = False
				break

		if do_close:
			break

		extract_and_assert()

	# -----------  Wait for threads to join and perform a last extraction  -----------
	for t in threads:
		t.join()

	extract_and_assert()

	# -----------  Assert no items where lost  -----------
	assert count == len(to_put) * rounds * thread_N

	# End of test_COPP_Common_queue_thread_safe
	

# -----------  Logging  -----------
import logging

def test_logging(copp_server):
	assert len(copp_server.logger.handlers) == 1
	assert type(copp_server.logger.handlers[0]) is logging.StreamHandler
	assert copp_server.logger.level == logging.INFO
	copp_server.set_log_level(copp_server.log_levels.WARN)
	assert copp_server.logger.level == logging.WARN
