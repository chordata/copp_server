# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

import pytest
import time
from .. import COPP_server, defaults

# ======================================
# =           UDP networking           =
# ======================================

def test_UDP_server(udp_server, test_udp_client):
	assert not udp_server.receiving_thread.is_alive()
	udp_server.start()
	assert udp_server.sock.fileno() != -1
	assert udp_server.receiving_thread.is_alive()

	client, sock = test_udp_client
	client.start()
	time.sleep(0.2)
	assert udp_server.pending() > 0