# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

from .receive_sample_messages import receive_sample_messages, port
from . import COPP_server
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='COPP server' )
	parser.add_argument('-x','--example', action='store_true', help='Run the example')
	parser.add_argument('port', nargs='?', type=int, default=port,  help='The server PORT')
	args = parser.parse_args()

	if args.example:
		receive_sample_messages(args.port)
	else:
		udp_server = COPP_server.COPP_Server( port = args.port )
		udp_server.logger.setLevel(udp_server.log_levels.DEBUG)
		udp_server.start()