# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

# ===========================================
# =           COPP Server example           =
# ===========================================

# This example will create the server, a dummy client to send some sample packets,
# expose the received contents to the user and close.
# 
# In normal operation the packet fetching should be called periodically 
# within your program 

# ======  End of COPP Server example  =======


from . import COPP_server
from .sample_messages import send_sample_messages, port
import time



def print_packet_msgs(packet):
	print("\tMessages:")

	for msg in packet.get():
		print(msg)


def receive_sample_messages( port = port ):
	server = COPP_server.COPP_Server( port = port )
	# Set log level. The COPP server has conveniency imports of all the logging
	# level constants at COPP_Server.log_levels
	server.logger.setLevel(server.log_levels.INFO)
	# Set the extended packet represntation to True, this way it will show all the 
	# properties of the packets in the debug output
	server.set_extended_str_repr(True)
	# Start the server and spawn the receiving thread
	server.start()

	# Send some sample messages to the server
	# this function will send
	# - 3x plain messages to the DataTarget.COMM
	# - 2x Bundles to the DataTarget.Q, containing 3 messages each  
	time.sleep(0.1)
	send_sample_messages()
	time.sleep(0.1)

	for bun in server.get():
		# Testing one of the packet general properties allows you to tell it's type
		if not bun.time:
			title = "\n **This message is a Common_Packet, it contains several COPP_messages**"
			# The commom packet is an artificial bundle, created in the server. It contains all
			# the recently arrived plain messages as COPP_Message objects. 
			# The general properties are all None  
		else:
			title = "\n **This message is a Lazy_Packet, it contains several COPP_messages**"
			# The Lazy Packet is the COPP representation of an OSC bundle. It contains an overall
			# description of the bundle in it's general properties.
			# The contents of the bundle are stored unparsed. The first call to .get() will 
			# trigger the parsing of the contents, and deliver the messages as COPP_Messages objects


		banner = "="*len(title)
		print(banner + title + "\n" + banner)
		# The  properties inspection and messages fetching interface is
		# unified btw both kind of packets
		print(bun)
		print_packet_msgs(bun)
		print()

	server.close_and_wait()


if __name__ == '__main__':
	receive_sample_messages()