	# COPP server

The Chordata Open Pose Protocol (COPP) is a lightweight protocol intended for exchanging pose data coming from a jointed structure (as the human body) in the context of the [Chordata motion capture framework](http://chordata.cc).

It’s main purpose it to transmit data gathered by sensors attached to the body to the other services and devices participating in the processing of the capture data.

It relies upon UDP as transport layer, and OSC as the application layer, making it a fast and easily extensible protocol. The COPP is completely compatible with the OSC specification, but makes a different semantic interpretation of the OSC elements: Instead of requesting the invocation of remote methods, in the COPP messages are just containers of structured data.

The main difference of this server with other OSC implementations is the lack of a dispatching mechanism. In exchange the COPP server provides lazy bundle parsing.

The complete COPP specification is WIP and will be published soon.


## Usage

- After creating an instance of the server just call `.start()`. It will spawn a receiving thread and return inmediatelly.

- You can iterate over all the received packets at a certain point in time by calling `.get()`

- The server will retrive you the queued packets one by one. This packet can be of type `COPP_Commom_Packet` which contains a list of all the messages arrives as plain OSC messages. Or `COPP_Lazy_packet` which is an internal reprentation of the OSC bundle, with some general properties as `time` pre-parsed, and the raw_payload waiting to be parsed at the moment of the first call to `.get()`.

Both types of packet present an unified interface, allowing to tell between them by duck-typing.

### As module

To run a listening COPP server:

```
python -m COPP_server
```

To run the example
```
python -m COPP_server --example
```

## Example
_This example can be found at receive_sample_messages.py_

```python
from . import COPP_server
from .sample_messages import send_sample_messages, port
import time



def print_packet_msgs(packet):
	print("\tMessages:")

	for msg in packet.get():
		print(msg)


def receive_sample_messages( port = port ):
	server = COPP_server.COPP_Server( port = port )
	# Set log level. The COPP server has conveniency imports of all the logging
	# level constants at COPP_Server.log_levels
	server.logger.setLevel(server.log_levels.INFO)
	# Set the extended packet represntation to True, this way it will show all the 
	# properties of the packets in the debug output
	server.set_extended_str_repr(True)
	# Start the server and spawn the receiving thread
	server.start()

	# Send some sample messages to the server
	# this function will send
	# - 3x plain messages to the DataTarget.COMM
	# - 2x Bundles to the DataTarget.Q, containing 3 messages each  
	time.sleep(0.1)
	send_sample_messages()
	time.sleep(0.1)

	for bun in server.get():
		# Testing one of the packet general properties allows you to tell it's type
		if not bun.time:
			title = "\n **This message is a Common_Packet, it contains several COPP_messages**"
			# The commom packet is an artificial bundle, created in the server. It contains all
			# the recently arrived plain messages as COPP_Message objects. 
			# The general properties are all None  
		else:
			title = "\n **This message is a Lazy_Packet, it contains several COPP_messages**"
			# The Lazy Packet is the COPP representation of an OSC bundle. It contains an overall
			# description of the bundle in it's general properties.
			# The contents of the bundle are stored unparsed. The first call to .get() will 
			# trigger the parsing of the contents, and deliver the messages as COPP_Messages objects


		banner = "="*len(title)
		print(banner + title + "\n" + banner)
		# The  properties inspection and messages fetching interface is
		# unified btw both kind of packets
		print(bun)
		print_packet_msgs(bun)
		print()

	server.close_and_wait()


if __name__ == '__main__':
	receive_sample_messages()

```