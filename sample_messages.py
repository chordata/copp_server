# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

import socket
import time

port = 4567
server_address = ('localhost', port)


def send_sample_messages():
	""" This function creates a dummy UDP client to send a couple of test messages to the server.
	This low level funcionality is not required to make the server work. 
	Is just here to give us some data to work with in the example"""

	from .osc4py3_buildparse import oscbuildparse as oscparse
	
	test_timetag = time.time()
	test_timetag = oscparse.unixtime2timetag( 0.001 * test_timetag )
	test_target = "q"
	first_msg_addr = "/%%/Chordata/" + test_target

	test_subtargets = ["base", "dorsal", "head"]
	payload = (',ffff', [1, 2, 3, 4])
	subtargets = ['/%/{}'.format(sub) for sub in test_subtargets]
	msgs = [oscparse.OSCMessage(sub, *payload) for sub in subtargets]
	first_msg = [oscparse.OSCMessage(first_msg_addr, ",N", [None])]
	bun = oscparse.OSCBundle(test_timetag, first_msg + msgs)
	raw_bun = oscparse.encode_packet(bun)
	
	lorem = ["Lorem ipsum dolor sit amet,",
			"consectetur adipisicing elit.",
			"Quaerat ea quisquam repellat placeat"]

	comms = [oscparse.OSCMessage("/Chordata/comm", ",s", [lor]) for lor in lorem] 

	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.sendto(raw_bun, server_address)
	sock.sendto(raw_bun, server_address)
	for comm in comms:
		sock.sendto(oscparse.encode_packet(comm), server_address)
		
	sock.close()