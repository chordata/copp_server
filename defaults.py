# Chordata Open Pose Protocol (COPP) server  
# -- Pose data server for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
# Copyright 2019 Bruno Laurencich
#
# This file is part of Chordata Open Pose Protocol (COPP) server.
#
# COPP server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COPP server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with COPP server.  If not, see <https://www.gnu.org/licenses/>.

from enum import Enum

BASE_OSC_ADDR = "/Chordata"

UDP_BUF_SIZE = 4096
UDP_RECV_TIMEOUT = 1 #in seconds

COPP_ALLOW_NESTED_BUNDLES = False

COPP_CHECK_MESSAGES = True

EXTENDED_STR_REPR = False

PARSE_NON_COPP_BUNDLES = False

ports = {
	"remote_console": 3925,
	"headless_blender": 3926,
	"desktop_blender": 3826 
}

class DataTarget(Enum):
    # capture data
    Q = 'q'
    RAW = 'raw'
    REAL = 'real'
    EXTRA = 'extra'
    ERR = 'err'
    COMM = 'comm'

    # armature data
    ROT = 'rot'
    POS = 'pos'
    VEL = 'vel'
    DIST = 'dist'

Q_TYPETAG = ',ffff'
RAW_TYPETAG = ',iiiiiiiii'
VEC_TYPETAG = ',fff'
S_TYPETAG = ',s'

target_typetag_pairs = {
	DataTarget.Q : Q_TYPETAG,
	DataTarget.RAW : RAW_TYPETAG,
	DataTarget.REAL : RAW_TYPETAG,
	DataTarget.ROT : Q_TYPETAG,
	DataTarget.POS : VEC_TYPETAG,
	DataTarget.VEL : VEC_TYPETAG,
	DataTarget.ERR : S_TYPETAG,
	DataTarget.COMM : S_TYPETAG,
    # DataTarget.EXTRA is not checked!
}

class Global_Parameters_handler:
    on_osc_address_update = []

    def set_check_messages(self, val):
        global COPP_CHECK_MESSAGES
        COPP_CHECK_MESSAGES = val

    def set_allow_nested_bundles(self, val):
        global COPP_ALLOW_NESTED_BUNDLES
        COPP_ALLOW_NESTED_BUNDLES = val

    def set_base_osc_addr(self, val):
        global BASE_OSC_ADDR
        BASE_OSC_ADDR = val

        for handler in self.on_osc_address_update:
            handler(val)   

    def set_extended_str_repr(self, val):
        global EXTENDED_STR_REPR
        EXTENDED_STR_REPR = val

    def set_parse_non_copp_bundles(self, val):
        global PARSE_NON_COPP_BUNDLES
        PARSE_NON_COPP_BUNDLES = val